.PHONY: all

all: build

rebuild: clean build

build: venv
	./venv/bin/python freeze.py

venv:
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

clean: clean-venv clean-www

clean-venv:
	rm -rf ./venv

clean-www:
	rm -rf public

run:
	./venv/bin/python run.py
