import app
from flask_frozen import Freezer

freezer = Freezer(app.app)


if __name__ == '__main__':
    freezer.freeze()
