---
title: Zappos
startDate: 2015-01-01
endDate: 2016-08-30
jobTitle: Systems Automation Engineer
technologies:
 - Python
 - AWS
 - Puppet
 - Docker
---

###### Respondsibilities
- Deploy physical & virtual servers for internal use
- Constructed tools to increase team communication during incidents
- Dynamically generated email lists to ease company transition to holacracy
