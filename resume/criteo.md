---
title: Criteo
startDate: 2017-10-07
endDate: 2023-07-07
jobTitle: Senior Site Reliability Engineer
technologies:
 - Terraform
 - Python
 - Go
 - SaltStack
 - Chef
 - Ansible
 - Packer
 - Prometheus
 - TestKitchen
 - Jenkins
 - DataDog
 - AWS
 - Azure
 - Ubuntu
 - Centos
---


###### Responsibilities

- Hired in as Site Reliability Engineer and was promoted to a Senior in April 2020
- Manage all of Criteo's cloud infrastructure
- Work to integrate aquisitions into criteo network
- Maintain legacy cloud infrastructure
- Ease developer ability to utilize cloud resources on projects through
- Improved Observability on legacy platforms by implementing prometheus into the environment

###### Projects
- Design and built CICD pipeline for Terraform code
- Design and built BlueGreen CD system for Immutable Cloud Infrastructure deployments
- Expanding & stabilizing legacy platform to handle unprecidented growth
- Implemented quickly deployable Cloud Resources to allow for testing software builds before taking them to production
