---
title: Audigent / Experian
startDate: 2024-03-04
jobTitle: Senior DevOps Engineer
technologies:
 - Terraform
 - Python
 - Golang
 - Ansible
 - Prometheus
 - Grafana
 - Ubuntu
 - AWS
 - GCP
 - Cloudflare
 - Javascript
 - Haproxy
 - Nginx
 - Docker
 - Kubernetes
---

###### Responsibilities

- Lead a small DevOps team respondsible for managing the Audigent Real Time Data Platform
- Set team objectives and tracked progress assuring they line up with business priorities
- Managed integrations with other partners in AdTech Realm
- Solidifiy and productionize the real time data and publisher platformers
- Automate code Building & Release process for developers
- Enable developers to get code to production faster and safer, with easier rollbacks and less
- Ensure security practices are up to standards to ensure data security for our platform and the clients consuming it

###### Projects

- Investigate utilization of Kubernetes on baremetal servers to achieve near baremetal network performance in a high throughput, low latency environment to reduce cost and match demand curve closer
- Build out monitoring platform, setting up a team oncall rotation and establishing runbooks to assist on call handle incidents in real time
- Establish Terraform and ansible practices to make scale up simple and reduce blast redius on failures
