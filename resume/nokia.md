---
title: Nokia
startDate: 2016-09-01
endDate: 2017-09-30
jobTitle: Systems Automation Engineer
technologies:
 - Python
 - Terraform
 - SaltStack
 - Ubuntu
 - Jenkins
 - AWS
 - SCons
 - HDFS
---

###### Responsibilities

- Deploy & scale infrastructure for clients
- Participate in on call to resolve production issues in a timely manner
- Support developers needs as software needs changed, investigating new technologies and adding them to our systems
- Maintaining our Configuration as Code Repository, making changes that improved release time and consistency

###### Projects

- Built an inhouse deployment framework to ease new releases for customers
- Built a scalable Jenkins slave cluster to more closely match developer demand for system resources
- Lead change from compiling software on systems from source to packaging and releasing packaged artifacts, which were automatically built from git tags
- Brought Infrstracture as Code to company to get a better handle on cloud resources being consumed
- Refactored companies Configuration as Code system to significantly speed up installs
