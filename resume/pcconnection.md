---
title: PC Connection
startDate: 2023-07-21
endDate: 2024-02-29
jobTitle: Senior Site Reliability Engineer
technologies:
 - Kubernetes
 - Terraform
 - Python
 - Ansible
 - Docker
 - Packer
 - Redhat
 - Azure
 - AzureDevOps
 - DataDog
 - Databricks
 - Hashicorp Vault
---

###### Responsibilities
- Managed critical infrastructure running PC Connections production ecosystem
- Responding to security incidents with the Azure Platform
- Managed kuberenetes releases of code from contractors
- Consulted the linux adminstration team on migration preperation to new datacenters

###### Projects
- Migration of legacy infrastrcuture to Azure Cloud
- Ingegration of Hashicorp Vault into Connections ecosystem for certificate & secret management
- Setup Databricks and started company migration to utilizing this for its data lake platform
