class Configuration(object):
    DEBUG = True
    FLATPAGES_ROOT = 'resume'
    FLATPAGES_EXTENSION = '.md'
    FREEZER_DESTINATION = 'public'
    FREEZER_BASE_URL = 'https://scgodbold.com'
