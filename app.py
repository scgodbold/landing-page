import datetime

from flask import Flask, render_template
from flask_flatpages import FlatPages

import config

app = Flask(__name__)
app.config.from_object(config.Configuration)
pages = FlatPages(app)


@app.route("/")
def index():
    now = datetime.datetime.now()
    return render_template("layouts/home.html", build_time=now)


@app.route("/resume/")
def resume():
    now = datetime.datetime.now()
    return render_template("layouts/resume.html", pages=pages, build_time=now)
